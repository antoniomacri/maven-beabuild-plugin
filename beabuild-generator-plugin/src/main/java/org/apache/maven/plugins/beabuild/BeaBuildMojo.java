package org.apache.maven.plugins.beabuild;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;

/**
 * @goal generate-beabuild
 * @phase install
 * @requiresDependencyResolution compile
 */
public class BeaBuildMojo extends AbstractMojo {

    /**
     * @component role="org.apache.maven.project.MavenProjectBuilder"
     */
    protected MavenProjectBuilder projectBuilder;

    /**
	 * @parameter expression="${project.build.directory}/"
	 * @readonly
	 * @required
	 */ 
	protected String destinationFile;

	/**
	 * @parameter expression="${project}"
	 * @required
	 */
	protected MavenProject project;
	
	/**
	 * @parameter expression="${project.basedir}"
	 * @readonly
	 * @required
	 */
	protected File basedir;
	
	//private variable
	private String df;
	
	@Override
	public void execute() throws MojoExecutionException {
		AbstractBeaBuildMojo mojo = null;
		String t = project.getArtifact().getType();
		if(t.equals("war")){
			mojo = new WarBeaBuildMojo(projectBuilder);
			df = destinationFile;
		}
		else if(t.equals("ear")){
			mojo = new EarBeaBuildMojo(projectBuilder);
			
			df = basedir.getAbsolutePath() + "/src/main/application/";
		}
		
		if(mojo != null){
			mojo.setBasedir(basedir);
			mojo.init(project);
			mojo.create(df);
		}
	}
}
