package org.apache.maven.plugins.beabuild;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.dependency.utils.DependencyStatusSets;
import org.apache.maven.plugin.dependency.utils.DependencyUtil;
import org.apache.maven.project.DefaultProjectBuilderConfiguration;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;

public class WarBeaBuildMojo extends AbstractBeaBuildMojo {
	
	private DependencyStatusSets results;

    public WarBeaBuildMojo(MavenProjectBuilder projectBuilder) {
        super(projectBuilder);
    }

	public void create(String destinationFile) throws MojoExecutionException {
		super.outputFile = new File(destinationFile+WAR_BEABUILD_FILENAME);

		info("Recupero tutte le dipendenze del modulo war.");
		results = this.getDependencySets(true);

		info("Creo il contenuto del file "+outputFile);
		String output = createContentFile();

		info("Scrivo il contenuto del file.");
		try {
			DependencyUtil.write(output, outputFile, getLog());
		} catch (IOException e) {
			throw new MojoExecutionException("Error during writing file "+WAR_BEABUILD_FILENAME+".", e);
		}
		info("File creato correttamente.");
	}

	/**
	 * Recupera le varie dipendenze del progetto e genera il contenuto del file
	 * @return
	 * @throws MojoExecutionException
	 */
	private String createContentFile() throws MojoExecutionException {
		StringBuffer sb = new StringBuffer();
		
		for (Iterator/*<Artifact>*/  i = results.getResolvedDependencies().iterator(); i
				.hasNext();) {
			Artifact artifact = (Artifact) i.next();
			String artifactFilename = null;

            if (outputAbsoluteArtifactFilename) {
                artifactFilename = calculateArtifactFileName(artifact);
			}

			if (artifact.getType().equals("jar")) {
				String row = formatPath(artifactFilename) 
						+ " = "+project.getArtifact().getArtifactId() + "-"
						+ project.getArtifact().getVersion()+".war/WEB-INF/lib/" + artifact.getFile().getName();
				info(row);
				sb.append(row+ "\n");
				
			}
		}

		return sb.toString();
	}
}
