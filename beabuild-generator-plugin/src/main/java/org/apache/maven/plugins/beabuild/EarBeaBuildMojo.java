package org.apache.maven.plugins.beabuild;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.dependency.utils.DependencyStatusSets;
import org.apache.maven.plugin.dependency.utils.DependencyUtil;
import org.apache.maven.project.MavenProjectBuilder;

public class EarBeaBuildMojo extends AbstractBeaBuildMojo {

    public EarBeaBuildMojo(MavenProjectBuilder projectBuilder) {
        super(projectBuilder);
    }

    // variabili private
	private DependencyStatusSets results;
	private List /* <Dependency> */dependencies;
	private String destinationFile;

	public void create(String destinationFile) throws MojoExecutionException {
		this.destinationFile = destinationFile;
		super.outputFile = new File(destinationFile + EAR_BEABUILD_FILENAME);

		info("Recupero tutte le dipendenze del progetto.");
		results = this.getDependencySets(true);
		dependencies = super.project.getDependencies();

		info("Creo il contenuto del file "+outputFile);
		String output = createContentFile();

		info("Scrivo il contenuto del file.");
		try {
			DependencyUtil.write(output, outputFile, getLog());
		} catch (IOException e) {
			throw new MojoExecutionException("Error during writing file " + destinationFile + EAR_BEABUILD_FILENAME + ".", e);
		}
		info("File creato correttamente.");
	}

	/**
	 * Recupera le varie dipendenze del progetto e genera il contenuto del file
	 * 
	 * @return
	 * @throws MojoExecutionException
	 */
	private String createContentFile() throws MojoExecutionException {
		StringBuffer sb = new StringBuffer();

		contentEarModule(sb);
        getWarBeaBuild(sb);
		contentEarDependency(sb);

		return sb.toString();
	}

	/**
	 * Per ogni tipo dipendenza war questo plugin si aspetta di trovare un file
	 * generato dal goal generator-war-beabuild<BR>
	 * Il contenuto del file viene aggiunto al beabuild delle dipendenze
	 * dell'ear
	 * 
	 * @param sb
	 * @throws MojoExecutionException
	 */
	private void getWarBeaBuild(StringBuffer sb) throws MojoExecutionException {
		for (int i = 0; i < dependencies.size(); i++) {
			Dependency dep = (Dependency) dependencies.get(i);
			String type = dep.getType();
			String projectDir = getProjectDir(dep.getArtifactId());
			if (type.equals("war") && projectDir!=null ) {
				String path = projectDir + "/target/" + WAR_BEABUILD_FILENAME;
                File file = new File(path);
                if (!file.exists()) {
                    String disco = path.substring(0, 1);
                    String p = path.substring(2);
                    file = new File(disco + p);
                }
                
				if (file.exists()) {
					BufferedReader input = null;
					try {
						input = new BufferedReader(new FileReader(file));
						String line = null;
						while ((line = input.readLine()) != null) {
							sb.append(line + System.getProperty("line.separator"));
						}
					} catch (FileNotFoundException e) {
						throw new MojoExecutionException("Error during writing file " + destinationFile + EAR_BEABUILD_FILENAME + ".", e);
					} catch (IOException e) {
						throw new MojoExecutionException("Error during writing file " + destinationFile + EAR_BEABUILD_FILENAME + ".", e);
					} finally {
						try {
							input.close();
						} catch (IOException e) {
							throw new MojoExecutionException("Error during writing file " + destinationFile + EAR_BEABUILD_FILENAME + ".", e);
						}
					}
				} else {
					  
					getLog().warn("*********************************************************************");
					getLog().warn("Could not find the dependecies's file for the project war:" + dep.getArtifactId() + ": " + file.toURI());
					getLog().warn("The split deply may not work correctly.");
					getLog().warn("*********************************************************************");
				}
			}
		}
	}

	private void contentEarDependency(StringBuffer sb) throws MojoExecutionException {
		for (Iterator /* <Artifact> */i = results.getResolvedDependencies().iterator(); i.hasNext();) {
			Artifact artifact = (Artifact) i.next();
			String artifactFilename = null;
			if (outputAbsoluteArtifactFilename) {
				try {
					artifactFilename = artifact.getFile().getAbsoluteFile().getPath();
				} catch (NullPointerException e) {
					throw new MojoExecutionException("Error creating .beabuild file", e);
				}
			}

			if (artifact.getType().equals("jar")) {
				StringBuilder row = new StringBuilder();
				
                info("Artifact: "+artifact.toString()+", type: "+artifact.getClass().getSimpleName());
                
				// When the artifact is in the source tree we get it as ActiveProjectArtifact
				if (calculateArtifactFileName(artifact)!=null) {
					String artifactFileName = calculateArtifactFileName(artifact);
					if (artifactFileName != null) {
						row.append(artifactFileName + " = APP-INF/lib/" + artifact.getArtifactId() + "-" + artifact.getVersion());
                        if( artifact.getClassifier()!=null ) {
                            row.append("-"+artifact.getClassifier());
                        }
                        row.append(".jar");
					}
				} else {
					row.append(artifactFilename + " = APP-INF/lib/" + artifact.getFile().getName());
				}
				info(row.toString());
                row.append(System.getProperty("line.separator"));
				sb.append(row.toString());

			}
		}
	}

	private void contentEarModule(StringBuffer sb) throws MojoExecutionException {
		for (int i = 0; i < dependencies.size(); i++) {
			Dependency dep = (Dependency) dependencies.get(i);
			String type = dep.getType();
			String projectDir = getProjectDir(dep.getArtifactId());
            if( projectDir != null ) {
                if (type.equals("war")) {
                    // 1
                    String row = formatPath(projectDir) + "/src/main/webapp = " + dep.getArtifactId() + "-" + dep.getVersion() + ".war";
                    info(row);
                    sb.append(row + System.getProperty("line.separator"));

                    // 2
                    row = calculateArtifactFileNameFromProjectDir(projectDir) + " = " + dep.getArtifactId() + "-" + dep.getVersion() + ".war/WEB-INF/classes";
                    info(row);
                    sb.append(row + System.getProperty("line.separator"));

                } else if ( type.equals("ejb") ) {
                    String row = calculateArtifactFileNameFromProjectDir(projectDir) + " = " + dep.getArtifactId() + "-" + dep.getVersion() + ".jar";
                    info(row);
                    sb.append(row + System.getProperty("line.separator"));
                } else if ( type.equals("rar") ) {
                    String row = calculateArtifactFileNameFromProjectDir(projectDir) + " = " + dep.getArtifactId() + "-" + dep.getVersion() + ".rar";
                    info(row);
                    sb.append(row + System.getProperty("line.separator"));
                }
            }
		}
	}
}
