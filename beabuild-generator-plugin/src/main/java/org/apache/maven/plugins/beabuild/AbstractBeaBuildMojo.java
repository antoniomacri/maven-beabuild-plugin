package org.apache.maven.plugins.beabuild;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.dependency.resolvers.ResolveDependenciesMojo;
import org.apache.maven.project.DefaultProjectBuilderConfiguration;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.interpolation.ModelInterpolationException;

public abstract class AbstractBeaBuildMojo extends ResolveDependenciesMojo{

    protected final MavenProjectBuilder projectBuilder;


    public static final String EAR_BEABUILD_FILENAME = ".beabuild.txt";
	public static final String WAR_BEABUILD_FILENAME = ".warbeabuild.txt";

	private File basedir;

    protected AbstractBeaBuildMojo(MavenProjectBuilder projectBuilder) {
        this.projectBuilder = projectBuilder;
    }

    abstract void create(String desstinationFile) throws MojoExecutionException;
	
	public void init(MavenProject project2) {
		super.project = project2;
		super.outputAbsoluteArtifactFilename = true;
		super.excludeScope = "provided";
		super.excludeTransitive = false;
		super.excludeTypes = "ejb,war";
		super.silent = false;
		super.overWriteIfNewer = true;
		super.overWriteReleases = false;
		super.overWriteSnapshots = false;
	}
	
	/**
	 * Formatta il percorso di input in base alla volonta di weblogic
	 * 
	 * @param artifactFilename
	 * @return
	 */
	protected String formatPath(String artifactFilename) {
        if( artifactFilename.charAt(1)==':') {
            String start = artifactFilename.substring(0, 1).replace("\\", "/");
            String end = artifactFilename.substring(3).replace("\\", "/");
            return start + "\\:/" + end;
        } else {
            return artifactFilename;
        }
	}
	
	/**
	 * Non è detto che l'artifact e la cartella abbiano lo stesso nome...
	 * @param artifact
	 * @return
	 * @throws MojoExecutionException
	 */
	protected String calculateArtifactFileName(final Artifact artifact)
			throws MojoExecutionException {

        final String artifactId = artifact.getArtifactId();

        String projectDir = getProjectDir(artifactId);

        String artifactFilename = null;

        if( projectDir!=null ) {
            info("projectDir: " + projectDir + " for artifact " + artifact.getArtifactId() + " of type " + artifact.getType());

            artifactFilename = calculateArtifactFileNameFromProjectDir(projectDir);

        } else {
            artifactFilename = formatPath(artifact.getFile().getAbsoluteFile().getPath());
        }

        return artifactFilename;
	}

    protected String calculateArtifactFileNameFromProjectDir(String projectDir) throws MojoExecutionException {
        String artifactFilename;
        try {
            MavenProject artifactProject = projectBuilder.build(new File(projectDir + "/pom.xml"), new DefaultProjectBuilderConfiguration());

            try {
                projectBuilder.calculateConcreteState(artifactProject, new DefaultProjectBuilderConfiguration());
            } catch(NoSuchMethodError err) {
                info("calculateConcreteState doesn't exists");
            }
            artifactFilename = artifactProject.getBuild().getOutputDirectory();
            info(artifactFilename);
        } catch (ProjectBuildingException e) {
            throw new MojoExecutionException("exception creating artifact project", e);
        } catch (ModelInterpolationException e) {
            throw new MojoExecutionException("exception interpolating artifact project", e);
        }
        return formatPath(artifactFilename);
    }

    protected String getProjectDir(final String artifactId) {

        //TODO: leggere bene !!!
        // Qui dovremmo riuscire a capire il nome della cartella del progetto
        // locale quando si tratta di un modulo, ancora non ho ben capito come fare
        /*
        if( false && project.getArtifactId().equals(artifactId) ) {
            return project.getFile().getParentFile().getAbsolutePath();
        }
         */

        File parentProject = basedir.getParentFile();
        File[] rightFolders = parentProject.listFiles(new FilenameFilter() {

            public boolean accept(File dir, String name) {
                if (dir.isDirectory()
                        && name.toUpperCase().equals(artifactId.toUpperCase())) {
                    return true;
                }
                return false;
            }
        });

        String projectDir = null;

        if (rightFolders.length == 0) {
            //throw new MojoExecutionException("Folder on filesystem for artifact " + artifact+ " not found.");
            getLog().warn("********** Folder on filesystem for artifact " + artifactId+ " not found.");
            getLog().warn("********** Artifact " + artifactId + " will not be added to the beabuild.txt file.");
        } else if (rightFolders.length == 1) {
            projectDir = rightFolders[0].getAbsolutePath();
        } else {
            getLog().warn("********** Found too many folders on filesystem for artifact "+ artifactId + ".");
            getLog().warn("********** Artifact " + artifactId + " will not be added to the beabuild.txt file.");
        }
        return projectDir;
    }

    public File getBasedir() {
		return basedir;
	}

	public void setBasedir(File basedir) {
		this.basedir = basedir;
	}
	
	protected void info(String msg) {
		getLog().info(msg);
	}
}
